import Vue from 'vue'
import VueRouter from 'vue-router'

import Main from '@/views/main.vue'
import Login from '@/views/login.vue'

import ComponentsShow from '@/views/components-show.vue'
import ComunidadeForm from '@/views/comunidade-form.vue'
import ComunidadeList from '@/views/comunidade-list.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'main',
    component: Main
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/show',
    name: 'show',
    component: ComponentsShow
  },
  {
    path: '/form',
    name: 'form',
    component: ComunidadeForm
  },
  {
    path: '/list',
    name: 'list',
    component: ComunidadeList
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
