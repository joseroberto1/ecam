#!/bin/bash

set -e

until PGPASSWORD=$DJANGO_DB_PASS psql -h $DJANGO_DB_HOST -p $DJANGO_DB_PORT -U $DJANGO_DB_USER $DJANGO_DB_NAME -c '\q'; do
  echo "Postgres is unavailable - sleeping"
  sleep 5
done

# Building migrations
echo "Building migrations"
python manage.py makemigrations

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Creating admin
echo "Creating admin"
python -m django_createsuperuser "$DJANGO_ADM_USER" "$DJANGO_ADM_PASS"

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000

